
const gitDownload = require('./gitDownload');
const path = require('path');
const os = require('os');
const fs = require('fs-extra');

module.exports = function getTagsForVersion (releaseTag) {
	var tmpDir = path.resolve(os.tmpdir(), 'fontoxml-release-' + (new Date().getTime()));

	return gitDownload({
		source: `ssh://git@bitbucket.org/liones/fontoxml-release.git`,
		dest: tmpDir,
		branch: releaseTag
	})
		.then(() => {
			var it = null;
			try {
				it = fs.readJsonSync(path.join(tmpDir, 'fonto-manifest.json')).dependencies
			} catch (e) {}

			if(!it)
				try {
					var deps = fs.readJsonSync(path.join(tmpDir, 'bower.json')).dependencies;
					return Object.keys(deps).reduce((clean, name) => {
						clean[name] = deps[name].substr(deps[name].indexOf('#') + 1);
						return clean;
					}, {})
				} catch (e) {}

			fs.removeSync(tmpDir);

			if (it) return it;

			throw new Error('Could not extract package tags from fontoxml-release#' + releaseTag);
		});
}
