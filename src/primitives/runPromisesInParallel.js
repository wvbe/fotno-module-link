'use strict';

/**
 * @type {Number}
 */
const MAX_PARALLEL_PROMISES = 25;

/**
 * @param {Function[]} remainingTaskRunners
 * @param {[]} results
 * @returns {Promise}
 */
function runTask (remainingTaskRunners, results) {
	return Promise.resolve(remainingTaskRunners.shift()())
		.then((result) => {
			results.push(result);

			return remainingTaskRunners.length > 0 && runTask(remainingTaskRunners, results);
		});
}

/**
 * @param {Function[]} taskRunners
 * @returns {Promise}
 */
function runPromisesInParallel (taskRunners) {
	if (taskRunners.length === 0) {
		return Promise.resolve([]);
	}

	const maxParallelPromises = Math.min(MAX_PARALLEL_PROMISES, taskRunners.length);

	const results = [];
	const remainingTaskRunners = taskRunners.slice().reverse();
	const runningTasks = [];

	for (let i = 0; i < maxParallelPromises; ++i) {
		runningTasks.push(runTask(remainingTaskRunners, results));
	}

	return Promise.all(runningTasks).then(() => results);
}

/**
 * @type {runPromisesInParallel}
 */
module.exports = runPromisesInParallel;